/*
 * ultrasonic.c
 *
 *  Created on: 27 gru 2013
 *      Author: jedrzej.karpiewski@gmail.com
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "hcsr.h"

void init_ultrasonics() {
	DDRA = 0b11110000; // set PA4, PA5, PA6, PA7 as outputs for hc-sr04 triggers
	PORTA = 0x00;
	
	DDRD &= 0b11110000; // set PD0, PD1, PD2, PD3 as inputs for hc-sr04 echoes
	PORTD |= 0b00001111; // set pull-up resistors for PD0, PD1, PD2, PD3  -  might be unnecessary
	
	configure_ultrasonics_interruptions();
	configure_timer0_for_read_ultrasonics_echo();
}

void configure_ultrasonics_interruptions(){	
	//rising edge invoke interruption procedure
	EICRA = (1 << ISC01 | 1 << ISC11 | 1 << ISC21 | 1 << ISC31);
	EICRA |= (1 << ISC00 | 1 << ISC10 | 1 << ISC20 | 1 << ISC30);
}

void configure_timer0_for_read_ultrasonics_echo()
{
	TIMSK |= (1 << OCIE0); // Enable interruption from TIMER0 counter overflow
	OCR0 = 32;
	TCCR0 |= (1 << WGM01); // set clear timer on compare match mode
}

void triger_ultrasonic_right(void) {
	EIMSK |= (1 << INT3);
	PORTA |= (1 << PA4); 
	_delay_us(10);		
	PORTA &= ~(1 << PA4);	
}

void triger_ultrasonic_left(void) {
	// Depends on wires connections it migth be also INT2 and PA5
	EIMSK |= (1 << INT0);
	PORTA |= (1 << PA7);
	_delay_us(10);
	PORTA &= ~(1 << PA7);
}

void triger_ultrasonic_forward(void) {
	EIMSK |= (1 << INT1);
	PORTA |= (1 << PA6); 
	_delay_us(10);		
	PORTA &= ~(1 << PA6);	
}

/*

triger_ultrasonic_forward();
if(ultrasonic_forward_echo_length == ULTRASONIC_WAVE_LENGTH_THRESHOLD)
{
	is_waiting_for_end_of_echo_forward = 1;
}
while(is_waiting_for_end_of_echo_forward){}
if(ultrasonic_forward_echo_length < ULTRASONIC_WAVE_LENGTH_THRESHOLD)
{
	MOTORS_FORWARD_BOTH;
	stop_prescaled_timer();
	prescaled_timer_wait_125ms = 0;
	is_prescaled_timer_on = 0;
	
	ultrasonic_forward_echo_length = ULTRASONIC_WAVE_LENGTH_THRESHOLD;
}

*/
/*
 * prescaled_timer3.c
 *
 * Created: 2014-02-03
 *  Author: jedrzej.karpiewski@gmail.com
 */ 
#include <avr/io.h>
#include "prescaled_timer.h"

void init_prescaled_timer()
{
	ETIMSK |= (1 << OCIE3A);	 // Enable interruption from TIMER3 counter overflow	
	OCR3A = 1953;				 //compare match after 125ms
	TCCR3B |= ((1 << WGM32));    // set clear timer on compare match mode
}

void start_prescaled_timer()
{
	TCCR3B |= ((1 << CS30) | (1 << CS32));
	TCNT3 = 0;
}

void stop_prescaled_timer()
{
	TCCR3B &= ~((1 << CS30) | (1 << CS32));
	TCNT3 = 0;
}

/*
 * sharp.c
 *
 * Created: 2014-01-22
 *  Author: jedrzej.karpiewski@gmail.com
 */ 
#ifndef SHARP_H_
#define SHARP_H_

#define LEFT_SHARP 1
#define MIDDLE_SHARP 2
#define RIGHT_SHARP 3
#define LEFT_SHARP_QUANTIZATION_LEVEL_THRESHOLD 90
#define MIDDLE_SHARP_QUANTIZATION_LEVEL_THRESHOLD 90
#define RIGHT_SHARP_QUANTIZATION_LEVEL_THRESHOLD 90

void init_sharp();
uint8_t read_sharps_quantization_level(uint8_t sharp_id);

#endif /* SHARP_H_ */
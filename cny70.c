/*
 * cny70.c
 *
 * Created: 2014-02-02 
 *  Author: jedrzej.karpiewski@gmail.com
 */ 
#include <avr/io.h>

void init_line_detectors()
{
	DDRC = 0x00;
	PORTC = 0xff; //transoptory z podciagnieciem
}
/*
		// simple white line detecting mode
		if((PINC & 0x0F) < 15) //    any cny70 whiteline detection
		{
			
			if((PINC & 0x03) < 3) // any backward cny70
			{
				if((PINC & 0x02) == 0) // left back
				{
					MOTORS_FORWARD_LEFT;
					_delay_ms(500);
				}
				if((PINC & 0x01) == 0) // right back
				{
					MOTORS_FORWARD_RIGHT;
					_delay_ms(500);
				}
				
				MOTORS_FORWARD_BOTH;
			}
						
			if((PINC & 0x0C) < 12) // any forward
			{
				if((PINC & 0x08) == 0) // left forward
				{
					MOTORS_BACK_RIGHT;
					_delay_ms(600);
				}			
				if((PINC & 0x04) == 0) // right forward
				{
					MOTORS_BACK_LEFT;
					_delay_ms(600);
				}
				MOTORS_FORWARD_BOTH;
			}
		}
*/
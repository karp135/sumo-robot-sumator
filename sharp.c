/*
 * sharp.c
 *
 * Created: 2014-01-22
 *  Author: jedrzej.karpiewski@gmail.com
 */ 
#include <avr/io.h>
#include "sharp.h"

void init_sharp(){
	ADCSRA |= ((1 << ADPS1) | (1 << ADPS2) | (1 << ADPS0)); // Set ADC prescaler to 128 - 125KHz sample rate @ 16MHz
	ADMUX |= (1 << REFS0); // Set ADC reference to AVCC
	ADMUX |= (1 << REFS1);
	ADMUX |= (1 << ADLAR); // Left adjust ADC result to allow easy 8 bit reading
	ADCSRA |= (1 << ADEN);  // Enable ADC
}

uint8_t read_sharps_quantization_level(uint8_t sharp_id)
{
		ADMUX  &= 0xE0;  //~(1<<MUX0 | 1<<MUX1);
		switch(sharp_id)
		{
			case MIDDLE_SHARP:
				ADMUX  |= (1<<MUX1);
			break;
			case RIGHT_SHARP:
				ADMUX  |= (1<<MUX1 | 1<<MUX0);
		}
		ADCSRA |= (1 << ADSC);  // Start A2D Conversions
		while(ADCSRA & (1<<ADSC)); //wait for conversion end
		uint8_t quantization_level = ADCH;
		return quantization_level;
}

/*
uint16_t read_sharps_quantization_level_uint16_t(uint8_t sharp_id)
{
	ADMUX  &= 0xE0; 
	switch(sharp_id)
	{
		case MIDDLE_SHARP:
		ADMUX  |= (1<<MUX1);
		break;
		case RIGHT_SHARP:
		ADMUX  |= (1<<MUX1 | 1<<MUX0);
	}
	ADCSRA |= (1 << ADSC);  // Start A2D Conversions
	while(ADCSRA & (1<<ADSC)); 	
	uint16_t quantization_level = ADC;
	return quantization_level;
}
*/
/*
uint16_t get_mean_of_n_adc_conversions(uint8_t sharp_id, uint8_t n)
{
	uint16_t quantization_level_mean = 0;
	for(uint8_t i = 0; i < n; i++)
	{
		quantization_level_mean += read_sharps_quantization_level(sharp_id);	
	}
	quantization_level_mean = quantization_level_mean / n;
	return quantization_level_mean;
}

*/
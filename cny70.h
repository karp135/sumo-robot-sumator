/*
 * cny70.h
 *
 * Created: 2014-02-03
 *  Author: jedrzej.karpiewski@gmail.com
 */ 

#ifndef CNY70_H_
#define CNY70_H_

#define IS_WHITE_LINE (PINC & 0x0F) < 15
#define IS_WHITE_LINE_FORWARD (PINC & 0x0C) < 12
#define IS_WHITE_LINE_FORWARD_LEFT (PINC & 0x08) == 0
#define IS_WHITE_LINE_FORWARD_RIGHT (PINC & 0x04) == 0
#define IS_WHITE_LINE_BACK (PINC & 0x03) < 3

void init_line_detectors();

#endif /* CNY70_H_ */
/*
 * ultrasonic.h
 *
 *  Created on: 27 gru 2013
 *      Author: jedrzej.karpiewski@gmail.com
 */

#ifndef ULTRASONIC_H_
#define ULTRASONIC_H_

#define ULTRASONIC_WAVE_LENGTH_THRESHOLD 770

void triger_ultrasonic_left(void);
void triger_ultrasonic_forward(void);
void triger_ultrasonic_right(void);

void init_ultrasonics();
void configure_ultrasonics_interruptions();
void configure_timer0_for_read_ultrasonics_echo();

#endif /* ULTRASONIC_H_ */

/*
 * motors.c
 *
 * Created: 2014-01-24
 *  Author: jedrzej.karpiewski@gmail.com
 */ 
#include <avr/io.h>
#include "motors.h"

void init_motors()
{
	DDRB = 0xFF; // pins of PORTB are outputs for controlling motors	
	configure_timer1_for_PWM_control();	
	TCCR1A |= (1 << COM1A1 | 1 << COM1B1); // start timer
	// motors won't start running until OCR1A and OCR1B are equal to 0
}

void configure_timer1_for_PWM_control()
{
	TCCR1A = 0;
	TCCR1B = 0;	
	ICR1 = 2500; // 100Hz PWM	
	TCCR1A |= (1 << WGM11);
	TCCR1B |= ((1 << CS11) | (1 << CS10) | (1 << WGM13) | (1 << WGM12) | (1 << WGM11)); //set fast PWM mode with compare to ICR1, prescaler 64
}
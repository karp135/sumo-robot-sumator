/*
* inz.c
*
* Created: 2014-01-20
*  Author: jedrzej.karpiewski@gmail.com
*/ 
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "motors.h"    
#include "sharp.h"     
#include "hcsr.h"        
#include "prescaled_timer.h"       
#include "cny70.h"                                                     


volatile uint8_t left_sharp_quantization_level = 0;
volatile uint8_t middle_sharp_quantization_level = 0;
volatile uint8_t right_sharp_quantization_level = 0;
volatile uint8_t sharp_detections_amount = 0;
volatile uint8_t which_sharp_detect_last = 0;

volatile uint8_t prescaled_timer_wait_125ms = 0; 
volatile uint8_t is_prescaled_timer_on = 0;

volatile uint16_t ultrasonic_echo_length = 0;
volatile uint16_t ultrasonic_echo_counter = 0;

volatile uint8_t is_ultrasonic_echo_receiving_forward = 0;
volatile uint8_t is_ultrasonic_echo_receiving_right = 0;
volatile uint8_t is_ultrasonic_echo_receiving_left = 0;

volatile uint8_t is_waiting_for_end_of_echo_forward = 0;
volatile uint8_t is_waiting_for_end_of_echo_right = 0;
volatile uint8_t is_waiting_for_end_of_echo_left = 0;

volatile uint16_t iterator = 0; 
volatile uint8_t temp = 0;

volatile uint16_t ultrasonic_left_echo_length = ULTRASONIC_WAVE_LENGTH_THRESHOLD;
volatile uint16_t ultrasonic_right_echo_length = ULTRASONIC_WAVE_LENGTH_THRESHOLD;
volatile uint16_t ultrasonic_forward_echo_length = ULTRASONIC_WAVE_LENGTH_THRESHOLD;

int main(void)
{	
	init_motors();
	MOTORS_FORWARD_BOTH;
	// OCR register control motor power. Scale is linear. Max value in this PWM configuration is 2500.
	OCR_RIGHT_MOTOR = 2500;
	OCR_LEFT_MOTOR = 2500;
	init_ultrasonics();
	init_sharp();
	init_line_detectors();
	init_prescaled_timer();
	sei (); // enable interruptions	

	while (1)
	{	
		// PINC description
		// bit 0 - right back 
		// bit 1 - left back
		// bit 2 - right forward
		// bit 3 - left forward
		temp = PINC;

		if((PINC & 0b00001110) < 14) // any cny70 white line detection
		{	
			if((PINC & 0x0A) < 10) //   any forward cny70 white line detection
			{
				if((PINC & 0x08) == 0) // forward left
				{
					MOTORS_BACK_RIGHT;
					prescaled_timer_wait_125ms = 3;
					start_prescaled_timer();
					is_prescaled_timer_on = 1;
				}			
				//if((PINC & 0x02) == 0) // forward right
				else
				{
					MOTORS_BACK_LEFT;
					prescaled_timer_wait_125ms = 3;
					start_prescaled_timer();
					is_prescaled_timer_on = 1;
				}
			}
			else
			{
				MOTORS_FORWARD_BOTH;
				stop_prescaled_timer();
				prescaled_timer_wait_125ms = 0;
				is_prescaled_timer_on = 0;
			}
		}

		left_sharp_quantization_level = read_sharps_quantization_level(LEFT_SHARP);
		middle_sharp_quantization_level = read_sharps_quantization_level(MIDDLE_SHARP);
		right_sharp_quantization_level = read_sharps_quantization_level(RIGHT_SHARP);

		sharp_detections_amount = 0;
		which_sharp_detect_last = 0;

		if(left_sharp_quantization_level > LEFT_SHARP_QUANTIZATION_LEVEL_THRESHOLD)
		{
			sharp_detections_amount++;
			which_sharp_detect_last = LEFT_SHARP; // left is 1
		}
		if(middle_sharp_quantization_level > MIDDLE_SHARP_QUANTIZATION_LEVEL_THRESHOLD)
		{
			sharp_detections_amount++;
			which_sharp_detect_last = MIDDLE_SHARP; // middle is 2
		}			
		if(right_sharp_quantization_level > RIGHT_SHARP_QUANTIZATION_LEVEL_THRESHOLD)
		{
			sharp_detections_amount++;
			which_sharp_detect_last = RIGHT_SHARP; // right is 3
		}

		if((sharp_detections_amount == 1) && (is_prescaled_timer_on == 0))
		{
			switch(which_sharp_detect_last)
			{
			case LEFT_SHARP:
				if(left_sharp_quantization_level > LEFT_SHARP_QUANTIZATION_LEVEL_THRESHOLD + 10)
				{
					MOTORS_FORWARD_RIGHT;
				}
				break;		

			case MIDDLE_SHARP:
				if(middle_sharp_quantization_level > MIDDLE_SHARP_QUANTIZATION_LEVEL_THRESHOLD + 10)
				{
					MOTORS_FORWARD_BOTH;
				}
				break;

			case RIGHT_SHARP:
				if(right_sharp_quantization_level > RIGHT_SHARP_QUANTIZATION_LEVEL_THRESHOLD + 10)
				{
					MOTORS_FORWARD_LEFT;
				}
				break;
			}
		}
		if(sharp_detections_amount == 2)
		{
			//MOTORS_FORWARD_BOTH;
			// algorithm might be changed by manipulating OCR register values					
			if(middle_sharp_quantization_level > MIDDLE_SHARP_QUANTIZATION_LEVEL_THRESHOLD)
			{
				if(left_sharp_quantization_level > LEFT_SHARP_QUANTIZATION_LEVEL_THRESHOLD)
				{
					MOTORS_FORWARD_RIGHT;
				}
				else  //right sharp detection
				{
					MOTORS_FORWARD_LEFT;
				}
				stop_prescaled_timer();
				prescaled_timer_wait_125ms = 0;
				is_prescaled_timer_on = 0;
			}
		}
		if(sharp_detections_amount == 3)
		{
			MOTORS_FORWARD_BOTH;
			stop_prescaled_timer();
			prescaled_timer_wait_125ms = 0;
			is_prescaled_timer_on = 0;
		}				

		if(sharp_detections_amount == 0)
		{
			triger_ultrasonic_right();
			if(ultrasonic_right_echo_length == ULTRASONIC_WAVE_LENGTH_THRESHOLD)
			{
				is_waiting_for_end_of_echo_right = 1;	
			}					
			while(is_waiting_for_end_of_echo_right){}	// wait for echo
			if (ultrasonic_right_echo_length < ULTRASONIC_WAVE_LENGTH_THRESHOLD)
			{
				MOTORS_TURN_AROUND_RIGHT;
				prescaled_timer_wait_125ms = 2;
				start_prescaled_timer();
				is_prescaled_timer_on = 1;
			}
			ultrasonic_right_echo_length = ULTRASONIC_WAVE_LENGTH_THRESHOLD;

			triger_ultrasonic_left();
			if(ultrasonic_left_echo_length == ULTRASONIC_WAVE_LENGTH_THRESHOLD)
			{
				is_waiting_for_end_of_echo_left = 1;
			}
			while(is_waiting_for_end_of_echo_left){}
			if (ultrasonic_left_echo_length < ULTRASONIC_WAVE_LENGTH_THRESHOLD)
			{
				MOTORS_TURN_AROUND_LEFT;
				prescaled_timer_wait_125ms = 2;
				start_prescaled_timer();
				is_prescaled_timer_on = 1;

			}
			ultrasonic_left_echo_length = ULTRASONIC_WAVE_LENGTH_THRESHOLD;
		}
		if((is_prescaled_timer_on == 1) && (prescaled_timer_wait_125ms == 0)) 
		{
			stop_prescaled_timer();
			MOTORS_FORWARD_BOTH;
		}						
	}
}

ISR (TIMER3_COMPA_vect) {
	prescaled_timer_wait_125ms--;     // 125ms of waiting elapsed
}

//for ultrasonic2 - LEFT
ISR (INT0_vect) {
	if (is_ultrasonic_echo_receiving_left == 0) {        //setup listening
		TCNT0=0;
		TCCR0 |= ((1 << CS00));
		is_ultrasonic_echo_receiving_left = 1;
		is_waiting_for_end_of_echo_left = 1;
		EICRA &= ~(1 << ISC00);
	}
	else                                                // echo arrived
	{
		TCCR0 &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));
		EIMSK &= ~(1 << INT0);
		EICRA |= (1 << ISC00);

		ultrasonic_left_echo_length = ultrasonic_echo_counter;
		ultrasonic_echo_counter = 0;
		is_ultrasonic_echo_receiving_left = 0;
		is_waiting_for_end_of_echo_left = 0;
	}
}

//for ultrasonic3 - FORWARD
ISR (INT1_vect) {
	if (is_ultrasonic_echo_receiving_forward == 0) {
		TCNT0=0; 
		TCCR0 |= ((1 << CS00)); 
		is_ultrasonic_echo_receiving_forward = 1;
		is_waiting_for_end_of_echo_forward = 1;
		EICRA &= ~(1 << ISC10);			
	}
	else
	{
		TCCR0 &= ~((1 << CS02) | (1 << CS01) | (1 << CS00)); 
		EIMSK &= ~(1 << INT1); 
		EICRA |= (1 << ISC10);

		ultrasonic_forward_echo_length = ultrasonic_echo_counter;
		ultrasonic_echo_counter = 0;
		is_ultrasonic_echo_receiving_forward = 0;
		is_waiting_for_end_of_echo_forward = 0;
	}
}

//for ultrasonic1 - RIGHT
ISR (INT3_vect) {
	if (is_ultrasonic_echo_receiving_right == 0) {
		TCNT0=0;
		TCCR0 |= ((1 << CS00));
		is_ultrasonic_echo_receiving_right = 1;
		is_waiting_for_end_of_echo_right = 1;
		EICRA &= ~(1 << ISC30);
	}
	else
	{
		TCCR0 &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));
		EIMSK &= ~(1 << INT3);
		EICRA |= (1 << ISC30);

		ultrasonic_right_echo_length = ultrasonic_echo_counter;
		ultrasonic_echo_counter = 0;
		is_ultrasonic_echo_receiving_right = 0;
		is_waiting_for_end_of_echo_right = 0;
	}
}

ISR (TIMER0_COMP_vect) {
	ultrasonic_echo_counter++;
}
/*
 * prescaled_timer.h
 *
 * Created: 2014-02-03
 *  Author: jedrzej.karpiewski@gmail.com
 */ 
#ifndef PRESCALED_TIMER_H_
#define PRESCALED_TIMER_H_

void init_prescaled_timer();
void start_prescaled_timer();
void stop_prescaled_timer();

#endif /* PRESCALED_TIMER_H_ */